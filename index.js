//Ex1:
const handleConvertArray = (array) => {
  let stringLength = 0;
  let arrayLengthAppear = [];
  let maxObjectLengthAppear = {};
  for (let i = 0; i < array.length; i++) {
    if (stringLength < array[i].length) {
      stringLength = array[i].length
      arrayLengthAppear.push({
        length: array[i].length,
        appear: 1
      })
    } else {
      arrayLengthAppear.find(item => item.length === array[i].length).appear++
    }
  }
  maxObjectLengthAppear = arrayLengthAppear.reduce(function(prev, current) {
    return (prev.appear > current.appear) ? prev : current
  });
  return array.filter(item => item.length === maxObjectLengthAppear.length)
}

console.log('hello: ', handleConvertArray(['a', 'ab', 'abc', 'cd', 'def', 'gh']));

//Ex2:
const handleSumTopTwo = (array) => {
  const maxNumber = Math.max.apply(null, array);
  const secondMaxNumber = array.sort(function(a, b) { return b - a; })[1];
  return maxNumber + secondMaxNumber;
}

console.log('hello: ', handleSumTopTwo([1, 4, 2, 3, 5]));